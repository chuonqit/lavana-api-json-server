const jsonServer = require("json-server");
const middlewares = jsonServer.defaults();
const server = jsonServer.create();
const queryString = require("query-string");
const router = jsonServer.router("db.json");

server.use(middlewares);

server.use(jsonServer.bodyParser);

server.use((req, res, next) => {
    if (req.method === "POST") {
        req.body.createdAt = Date.now();
        req.body.updatedAt = Date.now();
    } else if (req.method === "PUT" || req.method === "PATCH") {
        req.body.updatedAt = Date.now();
    }
    next();
});

router.render = (req, res) => {
    const headers = res.getHeaders();
    const headersTotalCount = headers["x-total-count"];
    if (req.method === "GET" && headersTotalCount) {
        const queryParams = queryString.parse(req._parsedUrl.query);
        const result = {
            data: res.locals.data,
            pagination: {
                _page: Number.parseInt(queryParams._page) || 1,
                _limit: Number.parseInt(queryParams._limit) || 10,
                _totalRows: Number.parseInt(headersTotalCount),
            },
        };
        return res.jsonp(result);
    }
    res.jsonp(res.locals.data);
};

server.use("/api", router);

const PORT = process.env.PORT || 5000;
server.listen(PORT, () => {
    console.log("JSON Server is running");
});
